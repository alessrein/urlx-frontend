import React, { Component } from 'react';
import './App.css';

import Home  from './pages/Home';
import Top100 from './pages/Top100';



class App extends Component {

  constructor(props) {

    super(props);

    this.changePage = this.changePage.bind(this);

    this.state = {
      currentPage: 'Home',
    }
    
  }

  changePage(page) {

    this.setState({
      currentPage: page,
    })

  }

  render() {

    return (
      <div className="App">
          { this.state.currentPage === 'List' 
            ? <Top100 go={this.changePage} />
            : <Home go={this.changePage}/>
          }
      </div>
    );

  }
}

export default App;
