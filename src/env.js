var env = {
    host: "http://127.0.0.1:8000",
    
    endpoint: {
        shrink: "/api/shrink",
        top100: "/api/top100",
        link: "/l"
    }
}
export default env;