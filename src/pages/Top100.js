import React, { Component } from 'react';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faLink} from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import env from '../env';
import OrangeLink from '../components/OrangeLink';
import Spinner from '../components/Spinner';

library.add(faLink);

export default class Top100 extends Component  {
    constructor(props)
    {
        super(props);

        this.state = {
            links: [],
        }
    }

    // fetch data when view is displayed
    componentDidMount() {
        fetch(`${env.host}${env.endpoint.top100}`)
        .then(res => res.json())
        .then(data => this.setState({links: data}))
        .catch(error => console.log(error))
    }

    render() {
        return (
          <div className="overlay ">
      
              <div className="container">
                  <h1 className="display-4 my-5 text-center">Top 100 Leaderboard</h1>

                    { this.state.links == 0 ? <Spinner /> : 
                        <div>
                            <LinkList list={this.state.links} />
                            <OrangeLink onClick={ e => this.props.go('Home')}>
                                <FontAwesomeIcon icon={faAngleLeft} /> Back 
                            </OrangeLink>
                        </div>
                    }
              </div>
          </div>
        );
    }
}



function LinkList(props)
{
    const list = props.list;

    return (
        <div className="card mb-3" >
            <ol className="list-group list-group-flush">
                {/* display the links array as <ListItem /> */}
                { list.map( (link, index) => <ListItem key={link.id} link={link} index={index} />) }
            </ol>
        </div>
    )
}

function ListItem(props)
{
    const link = props.link;
    const href = `${env.host}${env.endpoint.link}/${link.code}`

    return (
        <li key={link.id} className="list-group-item ">
            <div className="row">
                <div className="col mb-2 mb-lg-0">

                    <b>#{ props.index + 1 }</b>

                    &nbsp; { link.title } <br/>
                    
                    <small>
                        <FontAwesomeIcon icon={faLink} />
                        <a href={href}
                            title={link.url} 
                            className="text-orange"> 
                            { ' ' /* space */ } {href}
                        </a>
                    </small>
                </div>
                
                <div className="col text-lg-right">
                    <span className="col-12badge badge-primary badge-pill">
                        { link.visits } visits
                    </span>
                </div>
        
            </div>
        
        </li> 

    )
}
