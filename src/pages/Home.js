import React, { Component } from 'react';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight, faLink } from '@fortawesome/free-solid-svg-icons'
import UrlInput from '../components/UrlInput';
import UrlOutput from '../components/UrlOutput';
import OrangeLink from '../components/OrangeLink';
import Spinner from '../components/Spinner';

// add icons 
library.add( faLink);

class Home extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            shortUrl: "",
            loading: false,
        }

        this.setUrl = this.setUrl.bind(this);
        this.onRequest = this.onRequest.bind(this);
    }

    setUrl(url) {
        this.setState({shortUrl: ""}); // to reset the copy button
        this.setState({
            shortUrl: url,
            loading: false,
        });
    }

    onRequest() {
        this.setState({
            loading: true
        })
    }

    render () {
      return  (
        <div className="overlay d-flex align-items-center justify-content-center">
            <div className="jumbotron text-center slide-up">

                <h1 className="display-4">Hi, welcome to URLX!</h1>
                <p className="lead mb-5">An amazingly simple URL shortener.</p>
                
                <UrlInput handler={this.setUrl} onRequest={this.onRequest} />

                {/* dont show if the short link is not present */}
                { this.state.shortUrl.length > 0 && !this.state.loading
                    && <UrlOutput shortUrl={this.state.shortUrl} />
                }

                { this.state.loading && <Spinner className="mb-3" /> }
                
                <OrangeLink onClick={ e => this.props.go('List')}>
                    Top 100 board <FontAwesomeIcon icon={faAngleRight} />
                </OrangeLink>

            </div>
        </div>
      )
    };
}

  
export default Home;