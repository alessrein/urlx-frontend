
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import Clipboard from 'react-clipboard.js';


library.add(faCopy);

export default class UrlOutput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            copied: false
        }
    }


    render() {

        return (
            <div className="input-group mb-3">
                <input type="text" required 
                    className="form-control font-weight-bold " 
                    readOnly={true} 
                    value={this.props.shortUrl}/>
                         
    
                <div className="input-group-append">
                    <Clipboard className="btn btn-secondary" 
                        data-clipboard-text={this.props.shortUrl} 
                        onSuccess={e=>this.setState({copied:true})}>
                        <FontAwesomeIcon icon={faCopy} /> {this.state.copied ? 'Copied!' : "Copy"}
                    </Clipboard>
    
                </div>
            </div>
        )
    }
}

