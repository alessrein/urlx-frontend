import React, { Component } from 'react';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faLink} from '@fortawesome/free-solid-svg-icons';
import env from '../env';

library.add(faLink);


export default class UrlInput extends Component {

    constructor(props) {

        super(props);

        this.state = {
            url: "",
            errors: [],
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
        
    }


    // keep long url updated in the state for the shrink button.
    changeHandler(e) {
        
        this.setState({url: e.target.value});

    }

    callServer(url) {
        return fetch(`${env.host}${env.endpoint.shrink}/`, {
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'post',
            body: JSON.stringify({ url: url }),
            mode: 'cors'
        })
        .then( res => res.json() )
        
    }


    // handler for the shrink button
    clickHandler(e) {
        this.props.onRequest();
        
        // get short url from server
        this.callServer(this.state.url).then((data) => {

            console.log(data);

            if(data.errors) {
                this.setState({errors: data.errors})
                return;
            } else this.setState({errors: []})
            
            this.props.handler(`${env.host}${env.endpoint.link}/${data.code}`)
            
        })
        .catch(error => {
            this.state.error = error;
            console.error(error);
        });

    }

    render() {

        return (
            <div>
                <div className="input-group mb-3">
                    <input type="text" 
                        // display red border if there are errors
                        className={`form-control ` + (this.state.errors.length > 0 && 'border border-danger')} 
                        placeholder="http://www.example.com/" 
                        onChange={this.changeHandler} />
        
        
                    <div className="input-group-append">
                        <button className="btn btn-primary" 
                            onClick={this.clickHandler}>
                            <FontAwesomeIcon icon={faLink } /> Shrink!
                        </button>
                    </div>
                </div>
                
                <ErrorBox errors={this.state.errors} />

            </div>
        );
    }
}

function ErrorBox(props) {

    if (props.errors.length > 0) {

        var errorList = props.errors.map(msg => <li>{msg}</li>);
        
        return (
            <div className="alert alert-danger mb-3 text-left">
                <ul className="mb-0">
                    {errorList}
                </ul>
            </div>
        )

    } else return null;
}
