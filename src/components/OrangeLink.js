import React from 'react';

export default function OrangeLink (props)
{
    return (
        <a className="text-orange font-weight-bold d-block text-center" 
            href="#" onClick={props.onClick}>
            {props.children}
        </a>   
    )
}