# URLX (Frontend)
An amazingly simple URL shortener.

--------------------------------------------

## Setup

Install dependencies

`npm install`

When you start the backend server, copy the host and paste it inside the env.js file like so:
```js
var env = {
    host: "http://127.0.0.1:8000", // your backend host here
    
    endpoint: {
        shrink: "/api/shrink",
        top100: "/api/top100",
        link: "/l"
    }
}
export default env;
```

In the project directory, you can run:

`npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


`npm run build`

Builds the app for production to the `build` folder.
